package io.jesq.securitytestdemo;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
        throws Exception {
        auth
            .inMemoryAuthentication()
                .withUser("user")
                .password("{bcrypt}$2a$12$pJWf8XxbAKjMC1vUdhfV1eqSKgYg9mH.EZdmEi3Gq/biG95whCsX2")
                .roles("USER")
            .and()
                .withUser("admin")
                .password("{bcrypt}$2a$12$oR.2dC7Guy7aEABxBJSjaee24VQvlB8MmVf9tI3QOzhYs1YN3AkLu")
                .roles("USER", "ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .formLogin()
                .and()
            .logout()
            .and()
            .authorizeRequests()
                .antMatchers("/secure/**")
                    .authenticated()
                .antMatchers("/admin/**")
                    .hasRole("ADMIN")
                .anyRequest()
                    .permitAll();

    }
}
