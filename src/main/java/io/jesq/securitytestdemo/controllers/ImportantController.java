package io.jesq.securitytestdemo.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ImportantController implements ErrorController {

    @GetMapping("/")
    public String insecure(Model model) {
        model.addAttribute("message", "Anyone can see this!");
        return "greeting";
    }

    @GetMapping("/admin/")
    public String admin(Model model) {
        model.addAttribute("message", "Oh you're an admin");
        return "greeting";
    }

    @GetMapping("/secure/")
    public String secure(Model model) {
        model.addAttribute("message", "Oh you're logged in");
        return "greeting";
    }

    @GetMapping("/error")
    public String error(Model model) {
        model.addAttribute("message", "You're not allowed here!");
        return "greeting";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
