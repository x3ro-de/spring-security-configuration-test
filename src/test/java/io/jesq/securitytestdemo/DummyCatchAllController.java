package io.jesq.securitytestdemo;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Profile("DummyCatchAllController")
public class DummyCatchAllController {
    @RequestMapping(path = "/**", method = RequestMethod.GET)
    @ResponseBody
    public String catchAll() {
        return "DummyCatchallController#catchAll";
    }
}
