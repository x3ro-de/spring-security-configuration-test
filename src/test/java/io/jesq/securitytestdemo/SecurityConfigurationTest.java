package io.jesq.securitytestdemo;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlPattern;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(DummyCatchAllController.class)
@Import({SecurityConfiguration.class})
@ActiveProfiles({"DummyCatchAllController", "default"})
public class SecurityConfigurationTest {
    private MockMvc mockMvc;
    @Autowired private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        this.mockMvc =
            MockMvcBuilders.webAppContextSetup(this.webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void asAnonymous_ICanAccessPublicResources() throws Exception {
        mockMvc.perform(get("/foobar")).andExpect(status().isOk());
        mockMvc.perform(get("/")).andExpect(status().isOk());
    }

    @Test
    public void asAnonymous_whenAccessingRestrictedPages_ImRedirectedToTheLoginPage() throws Exception {
        mockMvc.perform(get("/secure/foobar")).andExpect(redirectedUrlPattern("**/login"));
        mockMvc.perform(get("/secure/")).andExpect(redirectedUrlPattern("**/login"));
        mockMvc.perform(get("/admin/foobar")).andExpect(redirectedUrlPattern("**/login"));
        mockMvc.perform(get("/admin/")).andExpect(redirectedUrlPattern("**/login"));
    }

    @Test
    @WithMockUser(username = "user", roles = {"USER"})
    public void asLoggedInUser_ICanAccessSecureArea() throws Exception {
        mockMvc.perform(get("/secure/foobar")).andExpect(status().isOk());
        mockMvc.perform(get("/secure/")).andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "user", roles = {"USER"})
    public void asLoggedInUser_ICantAccessAdminArea() throws Exception {
        mockMvc.perform(get("/admin/foobar")).andExpect(status().isForbidden());
        mockMvc.perform(get("/admin/")).andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
    public void asAdminUser_ICanAccessAdminArea() throws Exception {
        mockMvc.perform(get("/admin/foobar")).andExpect(status().isOk());
        mockMvc.perform(get("/admin/")).andExpect(status().isOk());
    }
}
